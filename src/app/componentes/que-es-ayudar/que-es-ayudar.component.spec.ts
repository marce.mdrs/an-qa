import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QueEsAyudarComponent } from './que-es-ayudar.component';

describe('QueEsAyudarComponent', () => {
  let component: QueEsAyudarComponent;
  let fixture: ComponentFixture<QueEsAyudarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QueEsAyudarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QueEsAyudarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
