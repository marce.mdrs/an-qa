import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {
  
  public innerScrollY: number = 0;
  public f: string = '';
  public t: string = 'top';

  constructor(){

  }

  ngOnInit(): void {
  }

  @HostListener('window:scroll', ['$event'])
  public onScrollY(event: any) {
    //console.log(window);
    this.innerScrollY = window.scrollY
    //console.log(window.scrollY);
    if(this.innerScrollY >= 10){
      this.f = 'fixed';
      this.t = '';
    }else{
      this.t = 'top';
      this.f = '';
    }
  }


}
