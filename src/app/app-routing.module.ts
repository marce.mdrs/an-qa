import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IniComponent } from './componentes/ini/ini.component';
import { Error404Component } from './componentes/error404/error404.component';
import { ContactoComponent } from './componentes/contacto/contacto.component';
import { QuienesSomosComponent } from './componentes/quienes-somos/quienes-somos.component';
import { QueEsAyudarComponent } from './componentes/que-es-ayudar/que-es-ayudar.component';
import { PreguntasFrecuentesComponent } from './componentes/preguntas-frecuentes/preguntas-frecuentes.component';

const routes: Routes = [
  {path: '', component: IniComponent},
  {path: 'inicio', component: IniComponent},
  {path: 'quienes-somos', component: QuienesSomosComponent},
  {path: 'que-es-ayudar', component: QueEsAyudarComponent},
  {path: 'preguntas-frecuentes', component: PreguntasFrecuentesComponent},
  {path: 'contacto', component: ContactoComponent},
  { path: '**', component: IniComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
